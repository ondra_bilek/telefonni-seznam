<?php
$rm = new App\RecordManager;

$records = $rm->getAllRecords($_GET['s']);
?>

<div class="row">
    <div class="col s12 l10 offset-l1">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Seznam záznamů</span>
                <div class="row">
                    <form action="" method="get">
                        <input type="hidden" value="list" name="p">
                        <input type="hidden" value="search" name="action">
                        <div class="input-field col s12 l6 offset-l3 left">
                            <i class="material-icons prefix">search</i>
                            <input name="s" id="search" type="text" class="validate" value="<?php echo $_GET['s'] ?>">
                            <label for="search">Vyhledat</label>
                        </div>
                        <?php if (!empty($_GET['s'])): ?>
                            <a href="?p=list" class="btn amber darken-2 amber-text text-lighten-5 center-align"><i class="material-icons left">clear</i>Zrušit hledání</a>
                        <?php endif ?>
                    </form>
                </div>

                <div class="row">
                    <table class="stripped ">
                        <thead>
                            <tr>
                                <td>id</td>
                                <td>Křestní</td>
                                <td>Příjmení</td>
                                <td>Telefon</td>
                                <td>Akce</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (empty($records)): ?>
                                <tr>
                                    <td colspan="5" class="center-align grey-text">
                                        <b>Žádné výsledky</b>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <?php foreach ($records as $record): ?>
                                <tr> 
                                    <!--TODO zvýraznit podle čeho je ve výsledku-->
                                    <td><?php echo $record->id ?></td>
                                    <td><?php echo $record->firstName ?></td>
                                    <td><?php echo $record->lastName ?></td>
                                    <td><?php echo $record->phone ?></td>
                                    <td>
                                        <a href="?p=record&id=<?php echo $record->id ?>" class="btn">
                                            <i class="material-icons left">edit</i>
                                            Upravit
                                        </a>
                                        <a href="#confirm" class="btn deep-orange modal-trigger"
                                           left" js-record="<?php echo $record->id ?>">
                                           <i class="material-icons left">remove</i>
                                            Smazat
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="confirm" class="modal">
    <div class="modal-content">
        <h4>Opravdu smazat?</h4>
        <p>Opravdu chcete tento záznam smazat?</p>
    </div>
    <div class="modal-footer">
        <a href="?p=list&action=remove&id=" 
           class="waves-effect teal lighten-2 waves-green btn-flat remove-confirm white-text">
            Ano
        </a>
        <a href="#" class="modal-close waves-effect deep-orange btn-flat white-text">Ne</a>
    </div>
</div>

<script>

    $(document).ready(function () {
        var linkTmeplate = '?p=list&action=remove&id=';

        $('.modal').modal();

        $('.modal-trigger').click(function (e) {
            var id = $(e.target).attr('js-record');

            $('.remove-confirm').attr('href', linkTmeplate + id);
        });
    });

    function myFunction(id, link) {
        if (confirm('Opravdu to chceš smazat?')) {
            // Save it!
//            console.log(link);
            window.location.href = link;
        } else {
            // Do nothing!
//            console.log(0);
        }
    }
</script>