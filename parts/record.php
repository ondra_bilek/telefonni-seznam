<?php

namespace App;

use App\RecordManager;

$action = $_GET['action'];
$page = $_GET['p'];

$rm = new RecordManager;

/* @var $records Record  */
$record = $rm->getRecord($_GET['id']);

?>


<div class="row">
    <div class="col s12 m12 l8 offset-l2">
        <div class="card">
            <div class="card-content">
                <span class="card-title"><h2><?php echo empty($record->id) ? 'Přidat' : 'Upravit' ?></h2></span>



                <form action="?p=record&action=save" method="post">
                    <input type="hidden" name="id" value="<?php echo $record->id ?>">
                    <input type="hidden" name="action" value="save">

                    <div class="row">
                        <div class="input-field col s6">
                            <input type="text" name="firstName" id="firstName" 
                                   value="<?php echo $record->firstName ?>" required="true"
                                   data-length="35" class="validate">
                            <label for="firstName">křestní jméno</label>
                        </div>
                        <div class="input-field col s6">
                            <input type="text" name="lastName" id="lastName"
                                   value="<?php echo $record->lastName ?>"  required="true"
                                   data-length="35" class="validate">
                            <label for="lastName">příjmení</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <input type="text" name="phone" id="phone"
                                   value="<?php echo $record->phone ?>"  required="true"
                                   data-length="25" class="validate">
                            <label for="phone">telefon</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col s6">
                            <button type="submit"  class="btn-large">
                                <i class="material-icons left">save</i>
                                Uložit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('input[type=text]').characterCounter();
    });
</script>