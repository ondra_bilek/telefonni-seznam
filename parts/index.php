<?php

namespace App;

use App\RecordManager;

$rm = new RecordManager;

/* @var $records Record  */
$logs = $rm->getAllLogs();
?>

<!--<div class="row">
    <form action="" method="get">
        <input type="hidden" value="list" name="p">
        <input type="hidden" value="search" name="action">
        <div class="input-field col s8  m6  offset-s2 offset-m3 left">
            <i class="material-icons prefix">search</i>
            <input name="s" id="search" type="text" class="validate">
            <label for="search">Vyhledat záznam</label>
        </div>
    </form>
</div>-->

<div class="row">
    <div class="col s12 l8 offset-l2">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Log</span>
                <table class="stripped">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>starý stav</th>
                            <th>nový stav</th>
                            <th>operace</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($logs as $row => $log): ?>
                            <tr>
                                <td><?php echo $log->id ?></td>
                                <td>
                                    <?php echo $log->getOldState(); ?>
                                </td>
                                <td>
                                    <?php echo $log->getNewState(); ?>
                                </td>
                                <td>
                                    <?php echo $log->operation ?>
                                </td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>