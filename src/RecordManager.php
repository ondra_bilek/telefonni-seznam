<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

use App\Record;

/**
 * Handling with Record object
 *
 * @author bilekond
 */
class RecordManager
{

    private $db;

    const TABLE_RECORD = 'record',
            TABLE_LOG = 'log',
            COLUMN_ID = 'id',
            COLUMN_FIRST_NAME = 'firstName',
            COLUMN_LAST_NAME = 'lastName',
            COLUMN_PHONE = 'phone',
            COLUMN_RECORD_ID = 'recordId',
            COLUMN_OLD_STATE = 'oldState',
            COLUMN_NEW_STATE = 'newState',
            COLUMN_OPERATION = 'operation';

    public function __construct() {
        $this->db = new DB();
    }

    /**
     * Stores Record into DB. It decides whether to insert or update on based on the existence of 
     * $record->id. It also logs in the end into DB.
     * @param Record $record
     */
    public function storeRecord(Record $record) {
        if (empty($record->id)) {
            $this->insertRecord($record);

            $this->logChange(null, $record);
        } else {
            $oldRecord = $this->getRecord($record->id);

            $this->updateRecord($record);

            $this->logChange($oldRecord, $record);
        }
    }

    /**
     * Checks whether record with given ID exists or not.
     * @param int $id
     * @return boolean
     */
    public function idExists($id) {
        $query = sprintf('SELECT COUNT(%1$s FROM %2$s WHERE %3$s = \'%4$s\'', //
                self::COLUMN_ID, //
                self::TABLE_RECORD, //
                self::COLUMN_ID, //
                $id
        );

        /* @var $result \mysqli_result */
        $result = $this->db->query($query);

        if ($result->num_rows > 0) {
            return true;
        }

        return false;
    }

    /**
     * Get row from database basedd on given ID and makes Record object of it. Then it returns this object.
     * @param int $id
     * @return Record
     */
    public function getRecord($id) {
        if (empty($id)) {
            return new Record;
        }

        $query = sprintf('SELECT * FROM %1$s WHERE id = %2$s', //
                self::TABLE_RECORD, //
                $id);


        $row = $this->db->query($query)->fetch_assoc();

        return $this->row2Record($row);
    }

    /**
     * Removes record from DB. And logs the action into DB
     * @param int $id
     */
    public function removeRecord($id) {
        $oldRecord = $this->getRecord($id);

        $query = sprintf('DELETE FROM %s WHERE %s = "%s"', self::TABLE_RECORD, //
                self::COLUMN_ID, $id
        );

        $this->db->query($query);

        $this->logChange($oldRecord, null);
    }

    /**
     * Takes values from $_POST and returns Record object. If the $_POST contains non-empty ID, it loads 
     * Record from DB first, then it replaces its values with the one from $_POST to update it.
     * 
     * @return Record record
     */
    public function createRecordObjectFromPost() {
        $id = $_POST['id'];

        if (empty($id)) {
            $record = new Record;
        } else {
            $record = $this->getRecord($id);
        }

        foreach ($_POST as $key => $value) {
            $record->$key = $this->db->connection->real_escape_string($value);
            //TODO escape na HTML tagy
        }

        return $record;
    }

    /**
     * Returns all records from DB as array of Records.
     * 
     * @return Record[] all Records
     */
    public function getAllRecords($searchString = null) {
        if (empty($searchString)) {
            $query = $this->withoutSearchQuery();
        } else {
            $query = $this->withSearchQuery($searchString);
        }

        $result = $this->db->query($query);

        $return = array();

        while ($row = $result->fetch_assoc()) {
            $return[] = $this->row2Record($row);
        }

        return $return;
    }

    /**
     * returns array with all Logs from DB
     * 
     * @return Log[]
     */
    public function getAllLogs() {
        $return = array();

        $query = sprintf('SELECT * FROM %s', //
                self::TABLE_LOG);

        $result = $this->db->query($query);
        
        while ($row = $result->fetch_assoc()) {
            $return[] = $this->row2Log($row);
        }

        return $return;
    }

    /**
     * converts DB row to Log object. It has to be row from table "log".
     * 
     * @param array $row
     * @return \App\Log
     */
    private function row2Log($row) {
        $log = new Log();

        foreach ($row as $key => $value) {
            switch ($key) {
                case self::COLUMN_OLD_STATE:
                case self::COLUMN_NEW_STATE:
                    
                    $log->$key = $value === null ? null : json_decode($value, false);
                    break;
                default:
                    $log->$key = $value;
                    break;
            }
        }

        return $log;
    }

    /**
     * Inserts log to DB after CHANGE, ADD or REMOVE actions on any record in the DB. Action type is 
     * decided inside this function based on old and new values. objects are encoded to JSON.
     * 
     * @param Record|null $oldRecord
     * @param Record|null $newRecord
     */
    private function logChange($oldRecord, $newRecord) {

        $id = $oldRecord->id;

        if ($newRecord === null) {
            $operation = 'DELETE';
        } elseif ($oldRecord === null) {
            $operation = 'ADD';

            //we need to take newRecord ID in this case
            $id = $newRecord->id;
        } else {
            $operation = 'EDIT';
        }

        $query = sprintf("INSERT INTO %s (%s, %s, %s, %s) VALUES ('%s', '%s', '%s', '%s')", //
                self::TABLE_LOG, self::COLUMN_RECORD_ID, self::COLUMN_OLD_STATE, //
                self::COLUMN_NEW_STATE, self::COLUMN_OPERATION, //
                $id, json_encode($oldRecord, JSON_UNESCAPED_UNICODE), //
                json_encode($newRecord, JSON_UNESCAPED_UNICODE), //
                $operation
        );

        $this->db->query($query);
    }

    /**
     * Converts associative array with values from DB into Record. Array has to contain values a with keys 
     * from "record" table.
     * 
     * @param array $row
     * @return Record
     */
    private function row2Record(array $row) {
        $record = new Record;

        foreach ($row as $key => $value) {
            $record->$key = $value;
        }

        return $record;
    }

    /**
     * Query for all records without fulltext search.
     *  
     * @return string query
     */
    private function withoutSearchQuery() {
        return sprintf('SELECT * FROM %s', self::TABLE_RECORD);
    }

    /**
     * Query for all records with fulltext search.
     *  
     * @return string query
     */
    private function withSearchQuery($searchFor) {
        return sprintf('SELECT * FROM %s WHERE %s LIKE "%s" OR  %s LIKE "%s" OR  %s LIKE "%s" '
                . 'ORDER BY %s ASC, %s ASC, %s ASC', //
                self::TABLE_RECORD, //
                self::COLUMN_FIRST_NAME, '%' . $searchFor . '%', //
                self::COLUMN_LAST_NAME, '%' . $searchFor . '%', //
                self::COLUMN_PHONE, '%' . $searchFor . '%', //
                self::COLUMN_FIRST_NAME, self::COLUMN_LAST_NAME, self::COLUMN_PHONE
        );
    }

    /**
     * Create new record in DB from Record object.
     * @param Record $record
     */
    private function insertRecord(Record &$record) {
        $query = sprintf('INSERT INTO %s (%s, %s, %s) VALUES ("%s", "%s", "%s")', //
                self::TABLE_RECORD, //
                self::COLUMN_FIRST_NAME, self::COLUMN_LAST_NAME, self::COLUMN_PHONE, //
                $record->firstName, $record->lastName, $record->phone
        );

        $this->db->query($query);

        $record->id = $this->db->connection->insert_id;
    }

    /**
     * Updates Record in DB from Record object.
     * @param Record $record
     */
    private function updateRecord(Record $record) {
        $query = sprintf('UPDATE %s SET %s = "%s", %s = "%s", %s = "%s" WHERE %s = "%s"', //
                self::TABLE_RECORD, //
                self::COLUMN_FIRST_NAME, $record->firstName, //
                self::COLUMN_LAST_NAME, $record->lastName, //
                self::COLUMN_PHONE, $record->phone, //
                self::COLUMN_ID, $record->id
        );

        $this->db->query($query);
    }

}

class NotImplementedException extends \Exception
{
    
}
