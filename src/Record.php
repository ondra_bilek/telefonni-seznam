<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

/**
 * Description of Record
 *
 * @author bilekond
 */
class Record
{
    public 
            $id,
            $firstName = '',
            $lastName = '',
            $phone = '';
    
    const MAX_NAME_LEN = 35,
            MAX_PHONE_LEN = 25;

    
    public function toArray() {
        return array(
            'id'=> $this->id,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'phone' => $this->phone,
        );
    }
    
    /**
     * Checks whether the values fulfill given criterias.
     * names cannot be empty and cannot be longer than 35 chars
     * phone cannot be empty and longer than 25 chars
     * 
     * It returns array of verbose warnings for each violation of these rules.
     * 
     * @return string[]
     */
    public function validateValues(){
        $return = array();
        
        if (empty($this->firstName)) {
            $return[] = "fname empty";
        }
        
        if (strlen($this->firstName) > self::MAX_NAME_LEN) {
            $return[] = "fname too long";
        }
        
        if (empty($this->lastName)) {
            $return[] = "lname empty";
        }
        
        if (strlen($this->lastName) > self::MAX_NAME_LEN) {
            $return[] = "lname too long";
        }
        
        if (empty($this->phone)) {
            $return[] = "phone empty";
        }
        
        if (strlen($this->phone) > self::MAX_PHONE_LEN) {
            $return[] = "phone too long";
        }
        
        return $return;
    }
}
