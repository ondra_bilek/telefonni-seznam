<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

use mysqli;

class DB
{

    private
            $servername = "wm136.wedos.net",
            $username = "a42628_cgi",
            $password = "8a7b4uXe",
            $dbName = 'd42628_cgi';

    /**
     * 
     * @var mysqli
     */
    public $connection;

    public function __construct() {
        $this->connection = new mysqli($this->servername, $this->username, $this->password, $this->dbName);

        if ($this->connection->connect_error) {
            die("Connection failed: " . $this->connection->connect_error);
        }
        
        $this->connection->set_charset('utf8');
    }
    
    /**
     * Shortcut to connection->query
     * 
     * @param string $query
     * @return \mysqli_result
     */
    public function query($query){
        return $this->connection->query($query);
    }

}
