<?php

namespace App;

/**
 * Helper for rendering HTML page
 *
 * @author bilekond
 */
class Render
{

    /**
     * Whole head and start of body
     */
    function head() {
        include __DIR__ . '/../parts/head.php';
    }

    /**
     * main menu
     */
    function menu() {
        include __DIR__ . '/../parts/menu.php';
    }

    /**
     * end of body and footer
     */
    function foot() {
        include __DIR__ . '/../parts/foot.php';
    }

    /**
     * it choses file according to passed parameter and places it inside body
     * @param string $page name of the page
     * @return null
     */
    function body(string $page) {
        $file = __DIR__ . '/../parts/' . $page . '.php';

        if (file_exists($file)) {
            include $file;

            return;
        }

        include __DIR__ . '/../parts/404.php';
    }

}
