<?php
//include all .php files from src folder
foreach (glob(__DIR__ .'/*.php') as $filename)
{
    if (strrpos($filename, 'include.php' ) > 0) {
        continue;
    }
    
    require_once  $filename;
}