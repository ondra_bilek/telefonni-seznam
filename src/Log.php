<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

/**
 * Description of Log
 *
 * @author bilekond
 */
class Log
{

    public
            $id,
            $oldState = null,
            $newState = null,
            $operation = "";

    public function toArray() {
        return array(
            'id' => $this->id,
            'oldState' => $this->oldState,
            'newState' => $this->newState,
            'operation' => $this->operation,
        );
    }

    public function getOldState() {
        if (empty($this->oldState)) {
            return '-';

            return;
        }

        return sprintf('Křestní: <b>%s</b><br>'
                . 'Příjmení: <b>%s</b><br>'
                . 'Telefon: <b>%s</b><br>', //
                $this->oldState->firstName, $this->oldState->lastName, $this->oldState->phone
        );
    }

    public function getNewState() {
        if (empty($this->newState)) {
            return '-';

            return;
        }

        return sprintf('Křestní: <b>%s</b><br>'
                . 'Příjmení: <b>%s</b><br>'
                . 'Telefon: <b>%s</b><br>', //
                $this->newState->firstName, $this->newState->lastName, $this->newState->phone
        );
    }

}
