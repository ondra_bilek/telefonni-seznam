<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

use App\RecordManager;

/**
 * Actions in the beginning of page load
 *
 * @author bilekond
 */
class Actions
{

    /**
     *
     * @var RecordManager
     */
    public $rm;

    public function __construct() {
        $this->rm = new RecordManager;
    }
    
    public function callAction($page, $action, &$args = null) {
        $functionName = $page . ucfirst($action);
        
        if (method_exists($this, $functionName)) {
            $this->$functionName($args);
            
            return;
        }
        
        //TODO vypsat neplatnou akci
    }
    
    /**
     * Action Save on page Record.
     * 
     * It stores sent data into Record in DB. After successful action it redirects page to list of records.
     * 
     * @return null
     */
    private function recordSave() {
        $record = $this->rm->createRecordObjectFromPost();

        $validationResult = $record->validateValues();

        if (sizeof($validationResult) !== 0) {
            //NOT OK
            //TODO ulozit do SESSION
            return;
        }

        $this->rm->storeRecord($record);

        $_POST = null;
        
        header("Location: ?p=list&result=savedOK");
    }

    /**
     * Action Remove on page List. It removes rcord after clicking on the correct button.
     */
    private function listRemove() {
        $id = $_GET['id'];

        if (!empty($id)) {
            $this->rm->removeRecord($id);
            header("Location: ?p=list&result=removedOK");
        } else {
            header("Location: ?p=list&result=RemoveFailed&reason=noId");
        }
    }

}
