<?php

namespace App;

require_once './src/include.php';

use App\{Render, Actions};

//some request info
$page = getPageParameter();
$action = getActionParameter();
$args = getAllParams();

//actions before any rendering
$actions = new Actions;
$actions->callAction($page, $action, $args);


//rendering page
$render = new Render();

$render->head();

$render->menu();

$render->body($page);

$render->foot();

/**
 * Reads and returns page name from URL. IN case none is found, returns "index"
 * @return string page name
 */
function getPageParameter() {
    $arg = $_GET['p'];

    if (is_null($arg)) {
        return 'index';
    }

    return $arg;
}

/**
 * Return action name from URL. Empty is OK.
 * @return string action name
 */
function getActionParameter() {
    $arg = $_GET['action'];

    return $arg;
}

/**
 * Returning all URL query parameters in array as Key Value pairs
 * @return string[] URL parameters
 */
function getAllParams() {
    $return = array();

    foreach (explode('&', $_SERVER['QUERY_STRING']) as $keyValue) {
        $pair = explode('=', $keyValue);
        $return[$pair[0]] = $pair[1];
    }

    return $return;
}

